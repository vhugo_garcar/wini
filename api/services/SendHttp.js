sails.Http = require('machinepack-http');

module.exports = {

  request: function(url, endpoint, method, headers, body, callback) {
    sails.Http.sendHttpRequest({
      method: method,
      url: endpoint,
      baseUrl: url,
      body: body,
      enctype: 'application/json',
      headers: headers
    }).switch({
      // An unexpected error occurred.
      error: function(err) {
        callback(err);
      },
      // A non-2xx status code was returned from the server.
      non200Response: function(result) {
        callback(result);
      },
      // Unexpected connection error: could not send or receive HTTP request.
      requestFailed: function(err) {
        callback(err);
      },
      // OK.
      success: function(result) {
        callback(null, JSON.parse(result.body));
      }
    });
  }

};
