const sgMail = require('@sendgrid/mail');
module.exports = {
  send: (subject, message, to, callback) => {
    try {
      sgMail.setApiKey(sails.config.sendgrid.key);

      const msg = {
        to: to,
        from: sails.config.welcome.email,
        subject: subject,
        html: message,
      };

      sgMail.send(msg);
      callback();
    } catch (error) {
      callback(error);
    }
  }
};