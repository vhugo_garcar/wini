const nodemailer = require("nodemailer");
module.exports = {
  send: (subject, message, attachments, to, credentials, callback) => {
    try {
      var user = credentials ? credentials.user : sails.config.emailCredentials.email;
      var password = credentials ? credentials.password : sails.config.emailCredentials.password;
      var transporter = nodemailer.createTransport({
        service: "gmail",
        auth: {
          user: user,
          pass: password
        }
      });

      // Activar permiso de aplicaciones: https://myaccount.google.com/lesssecureapps
      const mailOptions = {
        from: sails.config.emailCredentials.email, // sender address
        to: to, // list of receivers
        subject: subject, // Subject line
        html: message, // plain text body
        attachments: attachments // attachment files
      };
      transporter.sendMail(mailOptions, (err, info) => {
        if (err) {
          callback(err);
        }
        console.log("Email sended to: ", to);
        callback();
      });
    } catch (error) {
      callback(error);
    }
  },

  welcome: () => {
    var message =
      '<p><img src="https://wini.mx/images/wini.png" alt="wini-logo" width="120" height="47" /></p>' +
      '<p>Estimado usuario,&nbsp;</p>' +
      '<p><strong>Bienvenido a <a href="http://Wini.mx" target="_blank" rel="noopener" data-saferedirecturl="https://www.google.com/url?q=http://Wini.mx&amp;source=gmail&amp;ust=1570745611161000&amp;usg=AFQjCNE1reowOnEf-1uozGIh7n5psXGeng">Wini.mx</a>, la plataforma que s&iacute; te ayuda y conf&iacute;a en ti.</strong></p>' +
      '<p>&nbsp;</p>' +
      '<p>&iexcl;Ya est&aacute;s muy cerca de obtener tu pr&eacute;stamo!</p>' +
      '<p>Revisamos tu informaci&oacute;n, y podemos ofrecerte cualquiera de las siguientes opciones:</p>' +
      '<p>(Elige la que deseas, y contesta el cuestionario)</p>' +
      '<p>&nbsp;</p>' +
      '<p><strong>Banco Azteca:</strong></p>' +
      '<ul>' +
      '<li>Pr&eacute;stamos de $1,000 a $12,000 pesos</li>' +
      '<li>Entrega en 24 - 48 horas</li>' +
      '<li>Haz clic aqu&iacute;: <a href="http://bit.ly/bazaprobado" target="_blank" rel="noopener" data-saferedirecturl="https://www.google.com/url?q=http://bit.ly/BAaprobado&amp;source=gmail&amp;ust=1570745611161000&amp;usg=AFQjCNEF-RItZgvb77077vPWzjE2vSEX9g">http://bit.ly/BAaprobado</a>, baja la app y completa el cuestionario</li>' +
      '<li>Tiempo estimado: 8 minutos</li>' +
      '</ul>' +
      '<p><strong>Kueski:</strong></p>' +
      '<ul>' +
      '<li>Pr&eacute;stamos de $1,000 a $2,000 (aumenta conforme pagues a tiempo)</li>' +
      '<li>Entrega en 2 horas</li>' +
      '<li>Haz clic aqu&iacute;: <a href="http://bit.ly/KUaprobado" target="_blank" rel="noopener" data-saferedirecturl="https://www.google.com/url?q=http://bit.ly/KUaprobado&amp;source=gmail&amp;ust=1570745611161000&amp;usg=AFQjCNFbR_cEOyEg6WwOWUFd9LR0ly2JkQ">http://bit.ly/KUaprobado</a>, y completa el cuestionario</li>' +
      '<li>Tiempo estimado: 6 minutos</li>' +
      '</ul>' +
      '<p>&nbsp;</p>' +
      '<p><strong>POR FAVOR NO RESPONDAS A ESTE CORREO, SI NECESITAS AYUDA O INFORMACI&Oacute;N ADICIONAL, ESCR&Iacute;BENOS EN FACEBOOK A TRAV&Eacute;S DE ESTE ENLACE: <a href="https://www.facebook.com/winimexico" target="_blank" rel="noopener" data-saferedirecturl="https://www.google.com/url?q=https://www.facebook.com/winimexico&amp;source=gmail&amp;ust=1570745611161000&amp;usg=AFQjCNFLIiBH3HxCBAgcgfodzXeItdO4jg">https://www.facebook.com/<wbr />winimexico</a></strong></p>' +
      '<p>&nbsp;</p>' +
      '<p><a href="http://Wini.mx" target="_blank" rel="noopener" data-saferedirecturl="https://www.google.com/url?q=http://Wini.mx&amp;source=gmail&amp;ust=1570745611161000&amp;usg=AFQjCNE1reowOnEf-1uozGIh7n5psXGeng">Wini.mx</a> es una empresa 100% mexicana, creada para ayudar a las personas a encontrar productos y servicios financieros honestos y reales. Nuestro servicio es 100% gratuito y nunca te pediremos datos personales ni bancarios. Si tienes alguna duda o queja, por favor comun&iacute;cate con nosotros.</p>';

    return message;
  },

  template: params => {
    var message = params.products ?
      '<table align="center" cellpadding="0" cellspacing="0" style="width:650px">' +
      "<tbody>" +
      "<tr>" +
      '<td style="width:40px;background:#0966B2;">&nbsp;</td>' +
      '<td align="left" style="width:570px; height:140px; background:#0966B2;">' +
      '<img alt="wini-logo" src="https://wini.mx/images/wini.png" width="150" />' +
      "</td>" +
      '<td style="width:40px;background:#0966B2;">&nbsp;</td>' +
      "</tr>" +
      "<tr>" +
      '<td style="width:40px;background:#0966B2;">&nbsp;</td>' +
      '<td style="width:570px;background:#fff;color:#565656; padding:20px 30px; font-size:14px">' +
      '<table align="center" cellpadding="0" cellspacing="0" style="width:500px">' +
      "<tbody>" +
      '<tr style="color: #000;">' +
      '<td style="padding:10px;">Este es un correo enviado desde <strong><span style="color:#0966B2;">Wini</span>' +
      '<span style="color:#F15B33">.</span><span style="color:#0966B2;">mx</span> </strong></td>' +
      "</tr>" +
      '<tr style="color: #000;">' +
      '<td style="padding:10px;"><strong>Nombre: </strong>' +
      params.name +
      "</td>" +
      "</tr>" +
      '<tr style="color: #000">' +
      '<td style="padding:10px;"><strong>Teléfono: </strong>' +
      params.phone +
      "</td>" +
      "</tr>" +
      '<tr style="color: #000">' +
      '<td style="padding:10px;"><strong>Email: </strong>' +
      params.email +
      "</td>" +
      "</tr>" +
      '<tr style="color: #000">' +
      '<td style="padding:10px;"><strong>Productos: </strong>' +
      params.products.join(", ") +
      "</td>" +
      "</tr>" +
      "</tbody>" +
      "</table>" +
      "</td>" +
      '<td style="width:40px;background:#0966B2;">&nbsp;</td>' +
      "</tr>" +
      "<tr>" +
      '<td style="width:40px; background:#0966B2;">&nbsp;</td>' +
      '<td align="center" style="width:570px;height:90px; background:#0966B2; padding:0 30px; font-size:14px; color: #fff; ">Wini 2019</td>' +
      '<td style="width:40px;background:#0966B2;">&nbsp;</td>' +
      "</tr>" +
      "</tbody>" +
      "</table>" :
      '<table align="center" cellpadding="0" cellspacing="0" style="width:650px">' +
      "<tbody>" +
      "<tr>" +
      '<td style="width:40px;background:#0966B2;">&nbsp;</td>' +
      '<td align="left" style="width:570px; height:140px; background:#0966B2;">' +
      '<img alt="wini-logo" src="https://wini.mx/images/wini.png" width="150" />' +
      "</td>" +
      '<td style="width:40px;background:#0966B2;">&nbsp;</td>' +
      "</tr>" +
      "<tr>" +
      '<td style="width:40px;background:#0966B2;">&nbsp;</td>' +
      '<td style="width:570px;background:#fff;color:#565656; padding:20px 30px; font-size:14px">' +
      '<table align="center" cellpadding="0" cellspacing="0" style="width:500px">' +
      "<tbody>" +
      '<tr style="color: #000;">' +
      '<td style="padding:10px;">Este es un correo enviado desde <strong><span style="color:#0966B2;">Wini</span>' +
      '<span style="color:#F15B33">.</span><span style="color:#0966B2;">mx</span> </strong></td>' +
      "</tr>" +
      '<tr style="color: #000;">' +
      '<td style="padding:10px;"><strong>Nombre: </strong>' +
      params.name +
      "</td>" +
      "</tr>" +
      '<tr style="color: #000">' +
      '<td style="padding:10px;"><strong>Teléfono: </strong>' +
      params.phone +
      "</td>" +
      "</tr>" +
      '<tr style="color: #000">' +
      '<td style="padding:10px;"><strong>Email: </strong>' +
      params.email +
      "</td>" +
      "</tr>" +
      '<tr style="color: #000">' +
      '<td style="padding:10px;"><strong>Mensaje: </strong>' +
      (params.message || "Sin mensaje") +
      "</td>" +
      "</tr>" +
      "</tbody>" +
      "</table>" +
      "</td>" +
      '<td style="width:40px;background:#0966B2;">&nbsp;</td>' +
      "</tr>" +
      "<tr>" +
      '<td style="width:40px; background:#0966B2;">&nbsp;</td>' +
      '<td align="center" style="width:570px;height:90px; background:#0966B2; padding:0 30px; font-size:14px; color: #fff; ">Wini 2019</td>' +
      '<td style="width:40px;background:#0966B2;">&nbsp;</td>' +
      "</tr>" +
      "</tbody>" +
      "</table>";
    return message;
  }
};