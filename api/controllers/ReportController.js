/**
 * ReportController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
  email: function(req, res) {
    var params = req.allParams();
    if (
      !params.startDate ||
      !params.endDate ||
      !params.emails ||
      !params.emails.length
    ) {
      return res.badRequest();
    }

    var startDate = new Date(params.startDate);
    var endDate = new Date(params.endDate);
    var emailRegex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

    if (!startDate.getDate() || !endDate.getDate()) {
      return res.badRequest();
    }
    if (
      !_.every(params.emails, email => {
        return emailRegex.test(email);
      })
    ) {
      return res.badRequest();
    }

    Report.excel(startDate, endDate, params.columns, (err, data) => {
      var startFormat = startDate
        .toISOString()
        .slice(0, 16)
        .replace(/T/g, ' ');
      var endFormat = endDate
        .toISOString()
        .slice(0, 16)
        .replace(/T/g, ' ');
      var attachment = data.count ? [{
          path: data.path
        }] :
        null;
      var message =
        'Reporte del día ' +
        startFormat +
        ' al ' +
        endFormat +
        '. \n Total de leads nuevos: ' +
        data.count;
      Email.send(
        sails.config.report.subject,
        message,
        attachment,
        params.emails,
        null,
        err => {
          if (err) {
            console.log(err);
            console.log('Retry send report...');
            //TODO send report
          } else {
            var response = {
              count: data.count
            };
            return res.ok(response);
          }
        }
      );
    });
  },

  resend: async (req, res) => {
    let params = req.allParams();
    let report = await Report.findOne(params.id);
    var columns = report.columns.split(',').map(c => {
      return c.trim();
    });
    var now = new Date();
    var yesterday =
      params.startDate ||
      new Date(
        now.getFullYear(),
        now.getMonth(),
        now.getDate() - 1,
        report.hour,
        0,
        0,
        0
      );
    var today =
      params.endDate ||
      new Date(
        now.getFullYear(),
        now.getMonth(),
        now.getDate(),
        report.hour,
        0,
        0,
        0
      );
    Document.excel(
      yesterday,
      today,
      columns,
      report.repeatedData,
      (err, data) => {
        if (err) {
          console.log(err);
        } else {
          var subject = report.subject || 'Reporte ' + report.title;
          var message = `Reporte del ${yesterday.toLocaleString('es')}
          al ${today.toLocaleString('es')}.
          Total de leads nuevos: ${data.count}.`;
          if (!report.repeatedData) {
            message =
              message +
              ' Nota: El reporte omite los leads repetidos, basado en su correo.';
          }
          var attachments =
            data.count > 0 ? [{
              path: data.path
            }] :
            null;
          var emails = report.emails.split(',').map(c => {
            return c.trim();
          });
          Email.send(subject, message, attachments, emails, null, () => {
            Document.delete(data.path);
            return res.ok(report);
          });
        }
      }
    );
  },

  columns: (req, res) => {
    return res.ok(sails.config.report.columns);
  },

  download: function(req, res) {}
};