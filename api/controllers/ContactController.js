/**
 * ContactController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {

  email: function(req, res) {
    let params = req.allParams();
    var message = Email.template(params);
    var subject = (params.products) ? 'Anuncia tu marca Wini' : 'Contacto Wini';

    Email.send(subject, message, null, sails.config.emailContact.contact, null, function(err) {
      if (err) {
        console.log(err);
        return res.serverError(err);
      }
      return res.ok();
    });
  },

  welcome: async function(req, res) {
    let params = req.allParams();
    var subject = "Bienvenido a Wini.Mx";
    var message = Email.welcome();

    if (sails.config.welcome.active) {
      var leads = await Lead.find({
        email: params.email,
        phone: params.phone
      });

      if (leads.length <= 1) {
        SendGrid.send(subject, message, params.email, function(err, data) {
          if (err) {
            console.log(err);
            return res.serverError(err);
          }
          return res.ok(data);
        });
      } else {
        return res.ok();
      }
    } else {
      console.log('Welcome message is inactive');
      return res.ok();
    }
  }

};